//
//  Good.swift
//  Price Bar
//
//  Created by leo mac on 05.11.16.
//  Copyright © 2016 leo mac. All rights reserved.
//

import UIKit

class Good: NSObject {

    var barcode = ""
    var name = ""
    var price = 0.0
    var outlet = ""
    var favorite = false
    
    override var description: String {
        return "\(barcode) : \(name)"
    }
    
    override init() {
    
    }
     init(b : String, n : String, p : Double, o : String ) {
        barcode = b
        name = n
        price = p
        outlet = o
    }
}
