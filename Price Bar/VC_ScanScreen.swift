//
//  VC_ScanScreen.swift
//  Price Bar
//
//  Created by leo mac on 05.11.16.
//  Copyright © 2016 leo mac. All rights reserved.
//

import Foundation
import UIKit

class VC_ScanScreen : UIViewController {
    
    
    @IBOutlet weak var resultScanning: UILabel!
    
    
    @IBOutlet weak var barCodeField: UITextField!
    
    
    override func viewDidLoad() {
    
        
    
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let newGood = segue.destination as? VC_NewGood {
            
            
                newGood.barCode = barCodeField.text!
            
            
        }
    }
    
    @IBAction func btnScanImitation(_ sender: UIButton) {
        
        var found = false
        
        for g in goods {
            
            
            if barCodeField.text ==  g.barcode {
                resultScanning.text = "Есть такой в базе"
                found = true
                break
            }
            
        }
        if !found {
            //здесь нужен вызов Контроллера Добавления элемента в базу
            performSegue(withIdentifier: "showNewGood", sender: nil)
            
            
        }
        
        
        
    }
}
